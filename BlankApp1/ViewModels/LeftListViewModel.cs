﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace BlankApp1.ViewModels
{
	public partial class  LeftListViewModel : ObservableObject
    {

        public List<string> MusicItems { get; set; }

        /// <summary>
        /// 歌单列表展开或者关闭
        /// </summary>
        public RelayCommand MusicItemsOpenOrClose { get; set; }

        /// <summary>
        /// 通过是否可见来设置列表
        /// </summary>
        [ObservableProperty]
        private Visibility visibility;


        public LeftListViewModel()
        {
            Visibility = Visibility.Collapsed;
            MusicItems = new List<string>() {
                "五月天",
                "夜曲",
                "Deep Dark",
                "Link Park",
                "JoJo"
            };
            MusicItemsOpenOrClose = new RelayCommand(() => {
                if(Visibility == Visibility.Visible)
                {
                    Visibility = Visibility.Collapsed;
                }
                else
                {
                    Visibility = Visibility.Visible;
                }
            });
        }
	}
}
