﻿using BlankApp1.Services;
using BlankApp1.Views;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Microsoft.Extensions.DependencyInjection;
using Prism.Mvvm;
using System;
using System.Diagnostics;
using System.Windows;

namespace BlankApp1.ViewModels
{
    public partial class MainWindowViewModel : ObservableObject
    {
        private string _title = "Prism Application";
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        [ObservableProperty]
        private MainWindow mainWindow;

        /// <summary>
        /// 声明回调函数
        /// </summary>
        [ObservableProperty]
        private Action<string> mainTestShow;


        public TestB_Service TestB_Service { get; set; }



        public MainWindowViewModel()
        {
            //实例化回调函数
            MainTestShow = new Action<string>((msg) =>
            {
                Debug.WriteLine("主函数回调"+msg);
            });
            //直接从构造器里面拿到单例
            TestB_Service = App.ServiceProvider.GetService<TestB_Service>();
        }
    }
}
