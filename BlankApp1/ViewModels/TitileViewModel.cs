﻿using BlankApp1.Models;
using BlankApp1.Services;
using BlankApp1.Views;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using static BlankApp1.Views.MainWindow;

namespace BlankApp1.ViewModels
{
    public partial class TitileViewModel:ObservableObject
    {



        [ObservableProperty]
        private string title;


        [ObservableProperty]
        private string userName;

        //public List<>

        public IconButton IconButton { get; set; }

        public ResourceDictionary StyleResource { get; set; }

        public List<IconButton> IconButtonList { get; set; }

        public RelayCommand CloseWindow { get; set; }

        public RelayCommand MaxOrNormalWindow { get; set; }

        public RelayCommand MiniWindow { get; set; }

        public Action<string> TestCallBack { get; set; }
        
        public MainWindow MainWindow { get; set; }

        public TestB_Service TestB_Service { get; set; }
       


        public TitileViewModel() {

            StyleResource = new ResourceDictionary()
            {
                Source = new Uri("/BlankApp1;component/ViewStyles/TitleStyle.xaml",UriKind.RelativeOrAbsolute)
            };
            Title = "测试文本";

            IconButton = new IconButton()
            {
                Name = "EmailBox",
                Icon = "EmailBox",
                RelayCommand = new RelayCommand(() =>
                {
                    Debug.WriteLine("按钮测试");
                }),
                Style = (Style)StyleResource["SelectIcon"]
            };
            UserName = "用户名";

            CloseWindow = new RelayCommand(() => {
                MainWindow.Close();
                Debug.WriteLine("关闭窗口");
            });

            MaxOrNormalWindow = new RelayCommand(() => {
                if(MainWindow.WindowState == WindowState.Normal)
                {
                    MainWindow.WindowState = WindowState.Maximized;
                    MainWindow.MaxHeight = SystemParameters.MaximizedPrimaryScreenHeight;
                    MainWindow.MaxWidth = SystemParameters.MaximizedPrimaryScreenWidth;
                }
                else
                {
                    MainWindow.WindowState = WindowState.Normal;
                }
                Debug.WriteLine("最大化或正常窗口");
            });
            MiniWindow = new RelayCommand(() => {
                MainWindow.WindowState = WindowState.Minimized;
                TestCallBack("缩小窗口");
                
                Debug.WriteLine("缩小窗口");
                //WeakReferenceMessenger.Default.Send(new MainWindowMessage("缩小窗口"));
                //使用token双重推送
                WeakReferenceMessenger.Default.Send(new MainWindowMessage("缩小窗口"), MainWindowMessageToken.Test1.ToString());
            });
        }

    }
}
