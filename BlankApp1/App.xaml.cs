﻿using BlankApp1.Services;
using BlankApp1.ViewModels;
using BlankApp1.Views;
using Microsoft.Extensions.DependencyInjection;
using Prism.Ioc;
using System.Windows;

namespace BlankApp1
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {

        /// <summary>
        /// 全局静态IOC构造器
        /// </summary>
        public readonly static ServiceProvider ServiceProvider;

        /// <summary>
        /// 静态构造，这里以单例为主
        /// </summary>
        static App() {
            IServiceCollection services = new ServiceCollection();
            services.AddSingleton<TestB_Service>();
            services.AddSingleton<TestA_Service>(sp =>
            {
                return new TestA_Service()
                {
                    Name = "小兰"
                };
            });
            ServiceProvider = services.BuildServiceProvider();
            var res = ServiceProvider.GetService<MainWindowViewModel>();
        }

        /// <summary>
        /// 这里是启动端口，我们在这里进行IOC注入
        /// </summary>
        /// <returns></returns>
        protected override Window CreateShell()
        {
            
            return Container.Resolve<MainWindow>();
        }

        /// <summary>
        /// 这里是IOC容器的注入端口
        /// </summary>
        /// <param name="containerRegistry"></param>
        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
        }
    }
}
