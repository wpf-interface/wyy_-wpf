﻿using BlankApp1.Services;
using BlankApp1.ViewModels;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Diagnostics;
using System.Windows;

namespace BlankApp1.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        /// <summary>
        /// 消息订阅载体
        /// </summary>
        /// <param name="Msg"></param>
        public record MainWindowMessage(string Msg);
        public MainWindowViewModel ViewModel { get; set; }

        /// <summary>
        /// 主函数的Token
        /// </summary>
        public enum MainWindowMessageToken { Test1, Test2 }

        public MainWindow()
        {
            InitializeComponent();
            ViewModel = (MainWindowViewModel)DataContext;
            //var res  = App.ServiceProvider.GetService(typeof(MainWindowViewModel));
            //var res2 = App.ServiceProvider.GetService<MainWindowViewModel>();
            ViewModel.MainWindow = this;
            //ViewModel.TestB_Service = testB_Service;
            //ViewModel.MainWindow = new MainWindow();
            ViewModel.Title = "测试标题";
            //单一token
            //WeakReferenceMessenger.Default.Register<MainWindowMessage>(this,MessageRecieve);
            //双重token
            WeakReferenceMessenger.Default.Register<MainWindowMessage, string>(this, MainWindowMessageToken.Test1.ToString(), MessageRecieve);
           
            //也可以取消订阅，但是一般不用
            //WeakReferenceMessenger.Default.Unregister<MainWindowMessage>(this);

        }

        private void MessageRecieve(object recipient, MainWindowMessage message)
        {
            Debug.WriteLine(ViewModel.TestB_Service.TestA_Serivce.Name);
            Debug.WriteLine("接受到消息"+message.Msg);
        }

        private void MessageRecieve2(object recipient, MainWindowMessage message)
        {
            Debug.WriteLine("接受到消息2" + message.Msg);
        }

        private void Grid_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.DragMove();
        }

    }
}
