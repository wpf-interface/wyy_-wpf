﻿using BlankApp1.ViewModels;
using CommunityToolkit.Mvvm.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlankApp1.Utils;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BlankApp1.Services;

namespace BlankApp1.Views
{
    /// <summary>
    /// TitleView.xaml 的交互逻辑
    /// </summary>
    public partial class TitleView : UserControl
    {

        //这个只是为了代码提示，不涉及逻辑
        public MainWindow MainWindow { get; set; }

        public Action<string> TestCallBack { get; set; }

        //初始化依赖属性构造器
        public static readonly MyWpfExtension<TitleView> MyWpfExtension = new MyWpfExtension<TitleView>();

        //这个是简化后的依赖属性
        public static readonly DependencyProperty MainWindowProperty =
            MyWpfExtension.DependencyPropertySet<MainWindow>("MainWindow", (view, value) =>
        {
            view.TitileViewModel.MainWindow = value;
        });

        //将回调函数注入
        public static readonly DependencyProperty TestCallBackProperty =
            MyWpfExtension.DependencyPropertySet<Action<string>>("TestCallBack", (view, value) =>
            {
                view.TitileViewModel.TestCallBack = value;

            });



        /// <summary>
        /// DataContext的数据
        /// </summary>
        public TitileViewModel TitileViewModel { get; set; }

        public TestB_Service TestB_Service { get; set; }

        public TitleView()
        {
            InitializeComponent();
            //拿到DataContext数据重定向
            TitileViewModel = (TitileViewModel)DataContext;
            //TitileViewModel.TestB_Service = testB_Service;
            TitileViewModel.UserName = "小王";
        }
        public TitleView(TestB_Service testB_Service)
        {
            InitializeComponent();
            //拿到DataContext数据重定向
            TitileViewModel = (TitileViewModel)DataContext;
            TitileViewModel.TestB_Service = testB_Service;
            TitileViewModel.UserName = "小王";
        }
    }
}
