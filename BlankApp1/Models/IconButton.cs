﻿using CommunityToolkit.Mvvm.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BlankApp1.Models
{
    public class IconButton
    {
        public string Name { get; set; }
        public string Icon { get; set; }

        public RelayCommand RelayCommand { get; set; }

        public Style Style { get; set; }
    }
}
