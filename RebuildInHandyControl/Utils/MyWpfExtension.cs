﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace RebuildInHandyControl.Utils
{
    public class MyWpfExtension<View> where View : class
    {
        /// <summary>
        /// 简化依赖注入代码
        /// </summary>
        /// <typeparam name="View"></typeparam>
        /// <typeparam name="Value"></typeparam>
        /// <param name="name"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public DependencyProperty DependencyPropertySet<Value>(string name, Action<View, Value> action)
        {
            var res = DependencyProperty.Register(name, typeof(Value), typeof(View), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                new PropertyChangedCallback((item, res) =>
                {
                    var model = item as View;
                    var value = (Value)res.NewValue;
                    if (model != null)
                    {
                        action(model, value);
                    }
                    else
                    {
                        throw new Exception("model value is null");
                    }
                })));
            return res;
        }
    }
}
